$ mkdir bin
# Use avro to generate java code in src/main:
$ java -jar ./lib/avro-tools-1.7.7.jar compile schema event.avsc ./src/main
# Compile the source
$ javac -d': javac -d bin -cp lib/avro-tools-1.7.7.jar:lib/avro-1.7.7.jar:. src/main/com/rbh/avro/partner/acme/event/*
# Jar it up
$ cd bin
$ jar cf acmeStepEvent.jar com/
